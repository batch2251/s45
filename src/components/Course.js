import {Button, Card } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
// Destructure importation


export default function Course(course) {
    // Checks to see if the data was successfully passed
    // every component recieves information in a form of an object.
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isOpen, setIsOpen] = useState(true)

    const {name, description, price } = course.courseProp

    // if(count > 30) {
    //     alert("No More Seats")
    //     return false
    function enroll() {
        setCount(count + 1);
        setSeats(seats - 1)
        
    }

    useEffect(() => {

        if (seats === 0){
            setIsOpen(false)
            alert("No More slots");
            document.querySelector("#button").setAttribute('disabled', true)
        }
    }, [seats])

    return (
    
             
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text> Php {price}</Card.Text>
            {/*<Card.Text> Enrolless: {count}</Card.Text>*/}
            <Card.Text> Slots: {seats}</Card.Text>

            <Button id="button" variant="primary" onClick={enroll}>Enroll</Button>

        </Card.Body>
    </Card>
    

    )
}