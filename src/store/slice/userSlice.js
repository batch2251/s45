import { createSlice } from '@reduxjs/toolkit'

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    name: null,
    email: null,
    token: null,
    id: null,
    role: null
  },
  reducers: {
  	// userLogin
    userSet: (state, action) => {
      //console.log(action)	
      state.name = 'fefe'
      state.email = action.payload[0]
      state.token = 'asdasa56d4as654da5d4ad4asdsf654645645'
      state.id = 1
      state.role = 1
    },

    // userInfo
    userGet: (state) => {
      //console.log(action)	
      let email = localStorage.getItem('user')

      state.name = 'fefe'
      state.email = email
      state.token = 'asdasa56d4as654da5d4ad4asdsf654645645'
      state.id = 1
      state.role = 1
    },

    // userLogout
    userPurge: (state) => {
      state.name = null
      state.email = null
      state.token = null
      state.id = null
      state.role = null
    },
  },
})

export const { userSet, userGet, userPurge } = userSlice.actions
export default userSlice.reducer