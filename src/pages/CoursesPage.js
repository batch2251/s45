import { Row, Col } from 'react-bootstrap';
import Course from '../components/Course';
import coursesData from '../data/CoursesData';


export default function CoursesPage() {
	//  Checks to see if the mock data was captured

	console.log(coursesData);
	console.log(coursesData[0]);

	const courses = coursesData.map(course => {
	return (
		<Col>
			<Course key={course.id} courseProp = {course} />
		</Col>
	)
	})

	return(

		<>
		<Row>
			{courses}	
		</Row>
		</>
	)
}
