import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function Register() {
	// Activity
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	// Activity END


	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [street1, setStreet1] = useState('')
	const [street2, setStreet2] = useState('')
	const [city, setCity] = useState('')
	const [province, setProvince] = useState('')
	const [postalCode, setPostalCode] = useState('')

	const [email, setEmail] = useState('')
	const [password, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()

		fetch('https://capstone-2-477l.onrender.com/api/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNumber,
						email:email,
						password: password,
						address: {
							street1: street1,
							street2: street2,
							city: city,
							province: province,
							postalCode: postalCode,
						}
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result === true) {
				Swal.fire({
					title: 'Oops!',
					icon: 'error',
					text: 'Email already exist!'
				})
			} else {
				// ACTIVITY HERE
				fetch('https://capstone-2-477l.onrender.com/api/register', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNumber,
						email:email,
						password: password
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result);
					
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');

					setStreet1('')
					setStreet2('')
					setCity('')
					setProvince('')
					setPostalCode('')					

					if(result){
						Swal.fire({
							title: 'Register Successful!',
							icon: 'success',
							text: 'Salamat sa pag-rehistro!'
							})
						
						navigate('/login')

					} else {
						Swal.fire({	
							title: 'Registration Failed',
							icon: 'error',
							text: "Tropa, mukang mali ang iyong inilagay! :("
						})
					}		
				})
			}
		})

	}
	
	// function authenticate(event){
	// 	event.preventDefault(event)

	// 	localStorage.setItem('email', email)

	// 	setUser({
	// 		email: localStorage.getItem('email')
	// 	})

	// 	setEmail('')

	// 	navigate('/')
	// }

	// ACTIVITY END===============================================

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password !== '' && password2 !== '') && (password === password2)){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password, password2])

	return (
		(user.id !==null) ?
			<Navigate to="/courses"/>
		:
			<Form onSubmit={event => registerUser(event)}>

				<Form.Group controlId="firstName">
			        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter First Name"
				            value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Last Name"
				            value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="mobileNumber">
			        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Mobile Number"
				            value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required
				        />
			    </Form.Group>

			   	<Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				            type="email" 
				            placeholder="Enter email"
				            value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required
				        />
			        <Form.Text className="text-muted">
			            We'll never share your email with anyone else.
			        </Form.Text>
			    </Form.Group>

			    <Form.Group controlId="street1">
			        <Form.Label>street1</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter street1"
				            value={street1} 
				            onChange={event => setStreet1(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="street2">
			        <Form.Label>street2</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter street2"
				            value={street2} 
				            onChange={event => setStreet2(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="city">
			        <Form.Label>city</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter city"
				            value={city} 
				            onChange={event => setCity(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="province">
			        <Form.Label>province</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter province"
				            value={province} 
				            onChange={event => setProvince(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="postalCode">
			        <Form.Label>postalCode</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter postalCode"
				            value={postalCode} 
				            onChange={event => setPostalCode(event.target.value)}
				            required
				        />
			    </Form.Group>			    			    			    		    

	            <Form.Group controlId="password">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                value={password} 
				        onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="password2">
	                <Form.Label>Verify Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Verify Password" 
		                value={password2} 
				        onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>

	            {	isActive ? 
	            		<Button variant="primary" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			            :
			            <Button variant="primary" type="submit" id="submitBtn" disabled>
			            	Submit
			            </Button>
	            }
	            
			</Form>
	)
}